﻿// AlgorithmsMeasurements.cs
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace SortingAlgorithms
{
    public class Measurements
    {
        public long ExecutionTimeMilliseconds { get; }
        public double IterationCount { get; }
        public int SequanceSize { get; }
        
        public Measurements(long time, int count, int size)
        {
            ExecutionTimeMilliseconds = time;
            IterationCount = count;
            SequanceSize = size;
        }

        public override string ToString() => $"{SequanceSize};{ExecutionTimeMilliseconds};";
    }

    public delegate void SortDelegate<T>(IEnumerable<T> sequence, int start, int end);

    public static class AlgorithmsMeasurements
    {
        public static void RunAllTest()
        {
            var sortingAlg = new Dictionary<string, Action<int[]>>(){
                { "TimSort.txt", (int[] seq) => {
                    var alg = new TimSort<int>(seq);
                    alg.Sort(0, seq.Length - 1);
                }},
                { "StandartSort.txt", (int[] seq) => seq.ToList().Sort() },
                { "TreeSort.txt", (int[] seq) => Sort.TreeSort(seq)}
            };

            foreach (var title in sortingAlg.Keys)
            {
                RunTest("./input/AlmostSortedIntegers.txt", $"./output/almost_sorted/{title}", sortingAlg[title]);
                RunTest("./input/AscendingIntegers.txt", $"./output/ascending/{title}", sortingAlg[title]);
                RunTest("./input/DescendingIntegers.txt", $"./output/descending/{title}", sortingAlg[title]);
                RunTest("./input/UnsortedIntegers.txt", $"./output/unsorted/{title}", sortingAlg[title]);
            }
        }

        private static void RunTest(string input_path, string output_path, Action<int[]> sort)
        {
            var sequences = GetSequences(input_path);
            var result = new List<Measurements>();
            foreach (var seq in sequences)
            {
                Measurements measure = MeasureAlgorithm(() => sort(seq), seq.Length);
                result.Add(measure);          
            }
            WriteMeasuresInTheFile(output_path, result);
        }

        private static List<int[]> GetSequences(string path)
        {

            var sequences = new List<int[]>();
            var dataFromFile = Reader.ReadIntSequenceFromFile(path);
            var maxSize = 65536;
            for (int size = 32; size <= maxSize; size *= 2)
            {
                var copiedSeq = new int[size];
                Array.Copy(dataFromFile.Take(size).ToArray(), copiedSeq, size);
                sequences.Add(copiedSeq);
            }
            return sequences;
        }

        private static Measurements MeasureAlgorithm(Action sort, int size)
        {
            var stopwatch = new Stopwatch();
            int iterationCount = 0;
            stopwatch.Start();
            sort();
            stopwatch.Stop();      
            return new Measurements(stopwatch.ElapsedMilliseconds, iterationCount, size);
        }

        private static void WriteMeasuresInTheFile<T>(string path, IEnumerable<T> results) 
        { 
            using (StreamWriter file = new StreamWriter(path, false))
            {
                foreach(var result in results)
                    file.WriteLine(result.ToString());
            }  
        }
    }
}
