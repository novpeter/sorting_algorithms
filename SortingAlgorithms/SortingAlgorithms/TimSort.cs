﻿using System;
using System.Collections.Generic;

namespace SortingAlgorithms
{
    public class Range
    {
        public int Start { get; }
        public int End { get; }
        public int Length => End - Start + 1;

        public Range(int start, int end)
        {
            Start = start;
            End = end;
        }
    }

    public class TimSort<T> where T: IComparable 
    {
        public int IterationsCount { get; private set; }
        private T[] sequence { get; set; }

        public TimSort(T[] seq)
        {
            IterationsCount = 0;
            this.sequence = seq;
        }

        public void Sort(int start, int end)
        {
            var minRunLength = GetMinrun(end - start + 1);
            var runs = new List<Range>();
            var current = start;
            
            while (current <= end)
            {
                var run = GetRun(sequence, current, end);
                if (run.Length < minRunLength) {
                    var newEnd = Math.Min(end, run.Start + minRunLength - 1);
                    InsertionSort(sequence, current, newEnd);
                    run = new Range(run.Start, newEnd);
                }
                runs.Add(run);
                MergeCollapses(sequence, runs);

                current = run.End + 1;
            }  
            MergeAll(sequence, runs);
        }

        private Range GetRun(T[] sequence, int start, int end) 
        {
            if (start == end) return new Range(start, end);
            var current = start + 1;
            if (sequence[start].CompareTo(sequence[current]) <= 0)
                do {
                    ++current;
                    ++IterationsCount;
                } while (current <= end && sequence[current - 1].CompareTo(sequence[current]) <= 0);
            else {
                do {
                    ++current;
                    ++IterationsCount;
                } while (current <= end && sequence[current - 1].CompareTo(sequence[current]) > 0);
                Reverse(sequence, start, current - 1);
            }
            return new Range(start, current - 1);
        }

        private void Reverse(T[] sequence, int start, int end)
        {
            var length = end - start + 1;
            var buf = new T[length];
            var count = 0;
            for (var i = end; i >= start; i--)
            {
                IterationsCount++;
                buf[count++] = sequence[i];
            }
            for (var i = 0; i < buf.Length; i++)
            {
                IterationsCount++;
                sequence[start + i] = buf[i];
            }
        }

        private void MergeAll(T[] sequence, List<Range> runs)
        {
            while (runs.Count > 1)
                MergeTwoRuns(sequence, runs, runs.Count - 2);
        }
        
        private void MergeCollapses(T[] sequence, List<Range> runs)
        {
            while (runs.Count > 1) {
                var z = runs.Count - 3;
                var y = z + 1;
                var x = y + 1;
                if (runs.Count > 2 && runs[z].Length <= runs[y].Length + runs[x].Length)
                    MergeTwoRuns(sequence, runs, runs[z].Length <= runs[x].Length ? z : y);
                else if (runs[y].Length <= runs[x].Length)
                    MergeTwoRuns(sequence, runs, y);
                else
                    break;
            }
        }
        
        private void MergeTwoRuns(T[] sequence, List<Range> runs, int first)
        {
            var second = first + 1;
            Merge(sequence, runs[first].Start, runs[first].End, runs[second].End);
            runs[first] = new Range(runs[first].Start, runs[second].End);
            runs.RemoveAt(second);
        }

        private void Merge(T[] sequence, int start1, int end1, int end2)
        {
            // buffers for two parts of sequences
            var leftBuf = new T[end1 - start1 + 1];
            var rigthBuf = new T[end2 - end1];

            // Copy values in buffers
            for (var index = 0; index < leftBuf.Length; index++)
            {
                leftBuf[index] = sequence[start1 + index];
                IterationsCount++;
            }
            for (var index = 0; index < rigthBuf.Length; index++)
            {
                rigthBuf[index] = sequence[end1 + 1 + index];
                IterationsCount++;
            }

            int i = 0;
            int j = 0;
            int k = start1;

            while (i < leftBuf.Length && j < rigthBuf.Length) 
            {
                if (leftBuf[i].CompareTo(rigthBuf[j]) < 0)
                {
                    sequence[k] = leftBuf[i];
                    i++;
                }
                else
                { 
                    sequence[k] = rigthBuf[j];
                    j++;
                }
                k++;
            }
            
            while (i < leftBuf.Length)
            {
                sequence[k] = leftBuf[i];
                i++;
                k++;
            }
     
            while (j < rigthBuf.Length)
            {
                sequence[k] = rigthBuf[j];
                j++;
                k++;
            }
        }

        private void InsertionSort(T[] sequence, int start, int end)
        {
            for (int i = start + 1; i <= end; i++)
            {
                IterationsCount++;
                T cur = sequence[i];
                int j = i;
                while (j > start && cur.CompareTo(sequence[j - 1]) < 0)
                {
                    IterationsCount++;
                    sequence[j] = sequence[j - 1];
                    j--;
                }
                sequence[j] = cur;
            }
        }

        private int GetMinrun(int size)
        {
            int r = 0;
            while (size >= 64) {
                r |= size & 1;
                size >>= 1;
            }
            return size + r;
        }
    }
}

