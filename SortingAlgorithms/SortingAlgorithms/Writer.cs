﻿// Writer.cs
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace SortingAlgorithms
{
    public static class Writer
    {
        public static void CreateAscendingSequence()
        {
            var sequence = Enumerable.Range(1, (int)1e7);
            File.WriteAllText("./input/AscendindSortedIntegers.txt", String.Join("\n", sequence));
        }

        public static void WriteResult(IEnumerable<int> sequence, string path, string comment)
        { 
            using (StreamWriter file = new StreamWriter(path, false))
            {
                file.WriteLine(comment);
                foreach(var item in sequence)
                    file.WriteLine(item.ToString());
            }
        }

        public static void AppendText(string data, string path)
        { 
            using (StreamWriter file = new StreamWriter(path, true))
            {
                file.WriteLine(data);
            }
        }

        public static void CreateBigGraph()
        {
            var quantity = 1024;
            using (StreamWriter file = new StreamWriter("./input/Graph.txt"))
            {
                file.WriteLine(quantity);
                for (int i = 0; i < (quantity-2)/2; i++)
                    file.WriteLine($"{i} {2*i+1}\n{i} {2*i+2}");
            }
        }
    }
}
