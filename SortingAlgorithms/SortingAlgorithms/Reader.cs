﻿// Reader.cs
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace SortingAlgorithms
{
    public static class Reader
    {
        public static Graph ReadGraphFromFile(string path)
        {
            try
            {
                var data = File.ReadAllLines(path);
                var graph = new Graph(int.Parse(data[0]));
                data
                    .Skip(1)
                    .Foreach(line =>
                    {
                        var edge = line.Split(' ');
                        graph.Connect(int.Parse(edge[0]), int.Parse(edge[1]));
                    });
                return graph;           
            }
            catch (Exception e) { throw new Exception($"{e.Data}"); }
        }
        
        public static List<int> ReadIntSequenceFromFile(string path)
        {
            var result = new List<int>();
            using (StreamReader sr = new StreamReader(path))
            {
                while (true)
                {
                    string temp = sr.ReadLine();
                    if (temp == null) return result;
                    result.Add(int.Parse(temp));
                }
            }
        }

        public static List<int> ReadIntSubSequenceFromFile(int quantity, string path)
        {
            var result = new List<int>();
            using (StreamReader sr = new StreamReader(path))
            {
                for (int i = 0; i < quantity; i++)
                {
                    string temp = sr.ReadLine();
                    if (temp == null) break;
                    result.Add(int.Parse(temp));
                }
            }
            return result;
        }
    }
}
