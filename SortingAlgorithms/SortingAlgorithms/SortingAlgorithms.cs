﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SortingAlgorithms
{
    public static class Sort
    {
        /// <summary>
        /// Sorting given sequence using TreeSort algorithm.
        ///
        /// Average Case Time Complexity : O(n log n) 
        /// Adding one item to a Binary Search tree on average takes O(log n) time. 
        /// Therefore, adding n items will take O(n log n) time
        ///
        /// Worst Case Time Complexity : O(n2). 
        /// The worst case time complexity of Tree Sort can be improved by using a self-balancing binary search tree like Red Black Tree, AVL Tree.
        /// Using self-balancing binary tree Tree Sort will take O(n log n) time to sort the array in worst case.
        ///
        /// Auxiliary Space : O(n)
        /// </summary>
        /// <returns>The sorted sequence of integer numbers.</returns>
        /// <param name="sequence">Sequence.</param>
        public static IEnumerable<T> TreeSort<T>(IEnumerable<T> sequence) where T : IComparable
        {
            var binaryTree = new Tree<T>();
            foreach (var item in sequence)
                binaryTree.Insert(item);
            return binaryTree.GetSortedSequence();  
        }
    
        /// <summary>
        /// Topological Sort for given graph.
        /// Time complexity: For input graph G=(V,E) run time = O(|V| + |E|)
        /// </summary>
        /// <returns>The sequence of vertices.</returns>
        /// <param name="graph">Graph.</param>
        public static IEnumerable<int> TopologicalSort(Graph graph)
        {
            var nodesCount = 0;
            var edgesCount = 0;
            
            var result = new List<int>();
            var inDegrees = CalculateInDegrees(graph);
            var queue = new Queue<int>();

            for (int i = 0; i < inDegrees.Length; i++)
                if (inDegrees[i] == 0)
                    queue.Enqueue(i);
                    
            while (queue.Count != 0)
            {
                var currentNode = queue.Dequeue();
                nodesCount++;
                yield return currentNode;
                foreach (var adjNode in graph[currentNode].IncidentNodes)
                {
                    edgesCount++;
                    inDegrees[adjNode.NodeNumber]--;
                    if (inDegrees[adjNode.NodeNumber] == 0) 
                        queue.Enqueue(adjNode.NodeNumber);
                }
            }
            //Check, if there is a vertex with degree > 0, 
            //it means that there is a cycle in a graph
            if(inDegrees.Any(degree => degree != 0)) 
                throw new Exception("Graph has cycle");  
        }
        
        /// <summary>
        /// Calculates the in degrees for given graph.
        /// Time complexity: Initialize In-Degree array: O(|V| + |E|)
        /// </summary>
        /// <returns>The in degrees array.</returns>
        /// <param name="graph">Graph.</param>
        private static int[] CalculateInDegrees(Graph graph)
        {
            var inDegrees = new int[graph.Length];
            graph.Nodes.Foreach(node => node.IncidentNodes.Foreach(adjVertex => inDegrees[adjVertex.NodeNumber]++));
            return inDegrees;
        }
    }
}
