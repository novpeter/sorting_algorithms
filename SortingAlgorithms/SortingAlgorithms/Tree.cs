﻿using System.Collections.Generic;
using System;

namespace SortingAlgorithms
{
    public class Vertex<T>
    {
        public T Key { get; }
        public Vertex<T> Left { get; set; }            
        public Vertex<T> Right { get; set; }

        public Vertex(T key) => Key = key;
    }

    public class Tree<T> where T : IComparable
    {
        public Vertex<T> Root { get; private set; }
        public List<T> SortedSequence { get; private set; }
        public int IterationsCount { get; private set; }

        public Tree()
        {
            IterationsCount = 0;
            SortedSequence = new List<T>();
        }
    

        public void Insert(T value)
        {
            IterationsCount++;
            if (Root == null)
            {
                Root = new Vertex<T>(value);
                return;
            }
            Vertex<T> current = Root;
            while (true) 
            {
                if (value.CompareTo(current.Key) < 0)
                {
                    if (current.Left == null)
                    {
                        current.Left =  new Vertex<T>(value);
                        return;
                    }
                    current = current.Left;
                    IterationsCount++;

                }
                else
                {
                    if (current.Right == null) 
                    {
                        current.Right =  new Vertex<T>(value);
                        return;
                    }
                    current = current.Right;
                    IterationsCount++;
                }
            }         
        }

        public List<T> GetSortedSequence() 
        {
            SortedSequence = new List<T>();       
            InorderTraversal(Root);
            //Console.WriteLine($"{IterationsCount};");
            return SortedSequence;
        }
        
        /// <summary>
        /// Inorder traversal to get inorder sequence.
        /// </summary>
        /// <param name="root">Root vertex.</param>
        public void InorderTraversal(Vertex<T> root)
        {
            IterationsCount++;
            if (root == null) return;
            InorderTraversal(root.Left);
            SortedSequence.Add(root.Key);
            InorderTraversal(root.Right);
        }
    }
}
