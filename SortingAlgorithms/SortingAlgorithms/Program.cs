﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace SortingAlgorithms
{   
    public class MainClass
    {
        public static void Main(string[] args)
        {
            //var seq = Reader.ReadIntSubSequenceFromFile(60000, "./input/AscendingIntegers.txt").ToArray();
            
            //TopologicalSortExample();
            //TreeSortExample(seq);
            //TimSortExample(seq);
            
            //AlgorithmsMeasurements.RunAllTest();
        }

        private static void TimSortExample<T>(T[] seq) where T:IComparable
        {
            var algorithm = new TimSort<T>(seq);
            algorithm.Sort(0, seq.Count()-1);
            Console.WriteLine($"TimSort result:\n{String.Join("\n", seq)}\n");
        }

        public static void TopologicalSortExample() 
        {
            var graph = Reader.ReadGraphFromFile("./input/Graph.txt");
            var time = new Stopwatch();
            time.Start();
            Console.WriteLine($"TopSort result:\n{String.Join("\n", Sort.TopologicalSort(graph))}\n");
            time.Stop();
            Console.WriteLine(time.ElapsedMilliseconds);
        }

        public static void TreeSortExample<T>(IEnumerable<T> seq) where T:IComparable
        {
            Console.WriteLine($"TreeSort result:\n{String.Join("\n", Sort.TreeSort(seq))}");
        }
    }
}
