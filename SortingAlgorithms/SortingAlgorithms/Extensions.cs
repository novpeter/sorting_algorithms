﻿using System;
using System.Collections.Generic;

namespace SortingAlgorithms
{
    public static class Extenssions 
    { 
        public static void Foreach<T>(this IEnumerable<T> seq, Action<T> action)
        {
            foreach (var item in seq)
                action(item); 
        }    
    }
}
